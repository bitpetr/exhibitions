class Review < ApplicationRecord
  belongs_to :tender
  belongs_to :user
  has_many :comments, dependent: :destroy

  mount_uploaders :attachments, UfileUploader
  validates :comment, presence: true
end
