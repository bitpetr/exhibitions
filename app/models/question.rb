class Question < ApplicationRecord
  belongs_to :user
  belongs_to :tender
  has_many :answers, dependent: :destroy

  mount_uploader :ufile, UfileUploader

  validates :body, presence: true
end
