class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :review

  mount_uploader :ufile, UfileUploader
  # mount_base64_uploader :ufile, UfileUploader

  validates :body, presence: true
end
