class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tenders, dependent: :destroy
  has_one :profile, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :invitations, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy

  validates :name, :phone, :company, :agreement, :purpose, presence: true

  scope :partners, -> { where(partner: true).order('created_at DESC') }
  scope :clients, -> { where(partner: false).order('created_at DESC') }
end
