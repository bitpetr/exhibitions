document.addEventListener('DOMContentLoaded', () => {
    const buttonTogglerNode = document.querySelector('.js-detail-toggle');
    const detailBlock = document.querySelector('.tender-detail');

    const toggler = {
        node: buttonTogglerNode,
        isVisible: false,
        get text() {
            return this.isVisible ? 'Показать все условия' : 'Скрыть все условия';
        }
    };

    toggler.node.addEventListener('click', toggleTenderDetailBlock);

    function toggleTenderDetailBlock(evt) {
        toggler.node.textContent = toggler.text;

        if(!toggler.isVisible) {
            toggler.isVisible = true;
            detailBlock.classList.remove('is-hidden');
        } else {
            toggler.isVisible = false;
            detailBlock.classList.add('is-hidden');
        }

    }
});