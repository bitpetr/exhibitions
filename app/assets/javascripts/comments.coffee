# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('.new_comment').on 'ajax:success', (event) ->
    [data, status, xhr] = event.detail
    if data?.html_content
      if data?.html_content?.append
        to_append = data.html_content.append
        for k, v of to_append
          $(k).append v

      if data?.html_content?.errors
        to_errors = data.html_content.errors
        for k, v of to_errors
          $(k).html v

  $('.jsformcomment').on 'click', ->
    $('#comment_ufile').val('')
    $this = $(this)
    review_id = $(this).data()
    $('#comment-' + review_id.review_id).parent().removeClass('formHidden')



  $('.jsbuttonformcomment').on 'click', ->
    $this = $(this)
    $this.parent().parent().addClass('formHidden')

