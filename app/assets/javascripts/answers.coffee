# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $ ->
    $('.new_answer').on 'ajax:success', (event) ->
      [data, status, xhr] = event.detail
      if data?.html_content
        if data?.html_content?.append
          to_append = data.html_content.append
          for k, v of to_append
            $(k).append v

        if data?.html_content?.errors
          to_errors = data.html_content.errors
          for k, v of to_errors
            $(k).html v

  $('.jsformanswer').on 'click', ->
    $('#answer_ufile').val('')
    $this = $(this)
    question_id = $(this).data()
    console.log(question_id)
    $('#answer-' + question_id.question_id).parent().removeClass('formHidden')

