if @answer.ufile?
  @filename = link_to @answer.ufile.file.filename, @answer.ufile.url, target: '_blank'
  puts @filename
else
  nil
end

json.set! :html_content,

    append: {
        "#answers#{@answer.question_id}": "<h6><em>#{@answer.user.name}:</em></h6>
        <p class='answer-body'>#{@answer.body}
          <br />
          файл: #{@filename}
        </p>"
    },

    errors: {
        "#answer-errors#{@answer.question_id}": "#{@answer.errors.full_messages.join("\n")}"
    }
