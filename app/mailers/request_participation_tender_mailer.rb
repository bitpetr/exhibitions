class RequestParticipationTenderMailer < ApplicationMailer
  def request_participation
    @user = params[:user]
    @email = @user.email
    @partner_url = profile_url(params[:partner_id])
    @tender_url = tender_url(params[:tender_id])
    mail(to: @email, subject: 'Заявка на участие в тендере')
  end
end
