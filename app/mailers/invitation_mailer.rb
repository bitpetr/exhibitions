class InvitationMailer < ApplicationMailer
  def invitation
    @user = params[:user]
    @email = params[:email]
    @company = params[:company]
    @url = tender_url(params[:tender_id])
    mail(to: @email, subject: 'Приглашаем участвовать в тендере')
  end
end
