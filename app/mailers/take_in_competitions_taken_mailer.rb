class TakeInCompetitionsTakenMailer < ApplicationMailer
  def taken
    @partner = params[:partner]
    @email = @partner.email
    @tender_url = tender_url(params[:tender_id])
    mail(to: @email, subject: 'Можете участвовать в тендере')
  end
end
