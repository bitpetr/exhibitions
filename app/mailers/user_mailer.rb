class UserMailer < ApplicationMailer
  default to: -> { ['petr.khilenko@ya.ru', 'pkhilenko@gmail.com', 'jamiz@inbox.ru'] }

  def new_registration(user)
    @user = user
    mail(subject: 'Новая регистрация на выставки!')
  end
end
