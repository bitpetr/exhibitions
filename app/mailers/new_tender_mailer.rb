class NewTenderMailer < ApplicationMailer
  default to: -> { User.where(partner: true).pluck(:email) }, from: 'petrablog.ru@gmail.com'

  def new_tender(url, tender)
    @url = url
    @tender = tender
    mail(subject: 'Опубликован новый тендер!')
  end
end
