class RequestsForParticipationInTendersController < ApplicationController
  before_action :authenticate_user!

  def index
    if current_user.partner
      redirect_to tenders_path
    else
      @tender = Tender.find(params[:tender_id])
      take_in_competitions = @tender.take_in_competitions
      @partners = User.where("users.id in (?)", take_in_competitions.map(&:partner_id))
    end
  end

  def create
    @tender = Tender.find(params[:tender_id])
    @partner = User.find(params[:partner_id])
    @take_in_competition = TakeInCompetition.where(tender_id: @tender.id, partner_id: @partner.id)
    if @take_in_competition.update(taken: true)
      TakeInCompetitionsTakenMailer.with(partner: @partner, tender_id: @tender.id).taken.deliver_later
      redirect_to tender_requests_for_participation_in_tenders_path(@tender), success: 'Заказчик взят участником тендера'
    else
      flash.now[:danger] = 'Что-то пошло не так'
    end
  end
end
