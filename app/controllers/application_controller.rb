class ApplicationController < ActionController::Base
  layout :layout_by_resource

  add_flash_types :success, :danger, :info, :warning, :error, :alert, :notice

  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
      if resource.is_a?(Admin)
        admin_admin_path
      else
        tenders_path
      end
  end

  private

  def layout_by_resource
    if devise_controller?
      'auth'
    else
      'application'
    end
  end
end
