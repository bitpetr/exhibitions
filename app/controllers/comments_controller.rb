class CommentsController < ApplicationController
  def create
    @review = Review.find(params[:review_id])
    @comment = @review.comments.build(comment_params)
    @comment.user_id = current_user.id
    @comment.save
    render layout: false
  end

  private

    def comment_params
      params.require(:comment).permit(:body, :ufile)
    end
end
