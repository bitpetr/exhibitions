class ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user, except: [:new, :create]

  def show
    @profile = @user.profile
  end

  def new
    @profile = Profile.new
  end

  def create
    @profile = Profile.new profile_params
    @profile.user_id = current_user.id
    if @profile.save
      redirect_to profile_path(current_user), success: 'Профиль успешно создан'
    else
      flash[:danger] = 'Не удалось создать профиль'
      render :new
    end
  end

  def edit
    @profile = current_user.profile
  end

  def update
    if current_user.profile.update profile_params
      redirect_to profile_path(current_user), success: 'Профиль успешно обновлен'
    else
      flash[:danger] = 'Не удалось обновить профиль'
      render :edit
    end
  end

  def destroy
    current_user.profile.destroy
    redirect_to root_path, success: 'Профиль удален'
  end

  def invite
    if current_user.tenders
      @invitation = current_user.invitations.build
      @invitation.invitee_id = @user.id

      if @invitation.save
        InvitationMailer.with(user: @user,
                              email: @user.email,
                              company: current_user.company,
                              tender_id: params[:tender_id]).invitation.deliver_later
        flash[:success] = 'Исполнитель приглашен'
        redirect_to users_path
      else
        flash[:danger] = 'Что-то пошло не так, не удалось послать приглашение'
      end
    else
      flash[:danger] = 'У вас нет тендера'
      redirect_to users_path
    end
  end

  private

    def profile_params
      params.require(:profile).permit(:avatar, :avatar_cache, :remove_avatar)
    end

    def load_user
      @user = User.find(params[:id])
    end
end
