class QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_tender
  before_action :partner?
  before_action :find_question, only: [:edit, :update, :destroy]


  def new
    @question = Question.new
  end

  def create
    @question = current_user.questions.build(question_params)
    @question.tender_id = @tender.id

    if @question.save
      redirect_to tender_path(@tender), success: 'Вопрос задан'
    else
      flash[:danger] = 'Не удалось задать вопрос'
      render 'new'
    end
  end

  def edit
  end

  def update
    if @question.update(question_params)
      redirect_to tender_path(@tender), success: 'Вопрос обновлен'
    else
      flash[:danger] = 'Не удалось обновить вопрос'
      render 'edit'
    end
  end

  def destroy
    @question.destroy
    redirect_to tender_path(@tender)
  end

  private

  def question_params
    params.require(:question).permit(:body, :ufile)
  end

  def find_tender
    @tender = Tender.find(params[:tender_id])
  end

  def find_question
    @question = Question.find(params[:id])
  end

  def partner?
    redirect_to @tender unless current_user.partner?
  end
end
