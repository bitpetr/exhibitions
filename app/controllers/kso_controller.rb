class KsoController < ApplicationController
  layout 'kso', except: :portfolio

  def index
  end

  def portfolio
    render layout: 'application'
    end

  def bookkeeping
    render layout: 'application'
    end

  def orders
    render layout: 'application'
    end

  def portfolio_add
    render layout: 'application'
  end
end
