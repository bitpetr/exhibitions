class Admin::ProfilesController < ApplicationController
  before_action :authenticate_admin!
  layout 'admin'

  def show
    @user = User.find(params[:id])
    @profile = @user.profile
  end
end
