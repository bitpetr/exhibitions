class TendersController < ApplicationController
  before_action :authenticate_user!, except: :show
  before_action :load_model, only: %i[show edit update destroy]

  def index
    current_user.partner? ? @tenders = Tender.all : @tenders = current_user.tenders.all
  end

  def show
  end

  def new
    if current_user.partner
      redirect_to cabinet_path(current_user)
    else
      @tender = Tender.new
    end
  end

  def create
    @tender = current_user.tenders.build(tender_params)
    if @tender.save
      redirect_to @tender, success: 'Тендер успешно создан'
    else
      flash[:danger] = 'Не удалось создать тендер'
      render :new
    end
  end

  def edit; end

  def update
    if @tender.update tender_params
      redirect_to tender_path, success: 'Тендер успешно обновлен'
    else
      flash[:danger] = 'Не удалось обновить тендер'
      render :edit
    end
  end

  def destroy
    @tender.destroy
    redirect_to tenders_path, success: 'Тендер удален'
  end

  def invatee
    @user = User.find(params[:user_id])
    if current_user.partner
      @tenders = @user.tenders.where(public: true)
    else
      flash[:danger] = 'Вас пригласили?'
      redirect_to tenders_path
    end
  end

  private

    def tender_params
      params.require(:tender).permit(
        :budget, :floor, :region, :exhibition, :offer_counter, :obligatory_condition, :company_info,
        :message_count, :technical_specification, :company_info, :main_purpose_participation,
        :main_focus, :special_request, :lighting, :choice_materials, :electrical, :utility_room,
        :negotiation_zone, :additional_equipment, :showcases, :reception, :exhibited_products,
        :height_stand, :open_to_visitors, :image_saturation, :main_focus_stand_more, :area,
        :tender_number_participants, :term_delivery_design_project, :dates_event, :user_id
      )
    end

    def load_model
      @tender = Tender.find params[:id]
    end
end
