class TakeInCompetitionsController < ApplicationController
  before_action :authenticate_user!

  def create
    tender = Tender.find(params[:tender_id])
    if current_user.partner
      competition = TakeInCompetition.new(tender_id: tender.id, partner_id: current_user.id)
      if competition.save
        RequestParticipationTenderMailer.with(
            user: tender.user,
            partner_id: current_user.id,
            tender_id: tender.id
            ).request_participation.deliver_later
        redirect_to tender_path(tender.id), success: 'Ваша заявка на учачтие в тендере принята'
      else
        flash.now[:danger] = 'Что-то пошло не так'
        redirect_to tender_path(tender.id)
      end
    else
      redirect_to tender_path(tender.id)
    end
  end

  def update
  end

  private


end
