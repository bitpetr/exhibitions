class ChoiceClientPartnerController < ApplicationController
  def choice_page
  end

  def create
    session[:partner] = params[:partner]
    redirect_to new_user_registration_path
  end
end
