class AddFieldFromAnketaToTenders < ActiveRecord::Migration[5.2]
  def change
    add_column :tenders, :dates_event, :string
    add_column :tenders, :term_delivery_design_project, :string
    add_column :tenders, :tender_number_participants, :string
    add_column :tenders, :main_focus, :string
    add_column :tenders, :main_focus_stand_more, :string
    add_column :tenders, :image_saturation, :string
    add_column :tenders, :open_to_visitors, :string
    add_column :tenders, :height_stand, :string
    add_column :tenders, :reception, :text
    add_column :tenders, :exhibited_products, :text
    add_column :tenders, :howcases, :text
    add_column :tenders, :additional_equipment, :text
    add_column :tenders, :negotiation_zone, :text
    add_column :tenders, :utility_room, :text
    add_column :tenders, :electrical, :text
    add_column :tenders, :choice_materials, :text
    add_column :tenders, :lighting, :text
    add_column :tenders, :special_request, :text
  end
end
