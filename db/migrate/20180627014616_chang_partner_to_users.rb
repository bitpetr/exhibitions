class ChangPartnerToUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :partner, :boolean
    add_column :users, :partner, :boolean, default: false
  end
end
