class ChangeColumnsToTenders < ActiveRecord::Migration[5.2]
  def change
    remove_column :tenders, :term_delivery_design_project, :string
    remove_column :tenders, :tender_number_participants, :string

    add_column :tenders, :term_delivery_design_project, :datetime
    add_column :tenders, :tender_number_participants, :integer
  end
end
