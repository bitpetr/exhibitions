class AddAttachmentsToReviews < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :attachments, :json
  end
end
