class AddStatusToTender < ActiveRecord::Migration[5.2]
  # enum: open, selection of proposals, transformed to order
  def change
    add_column :tenders, :status, :integer, default: 1
  end
end
