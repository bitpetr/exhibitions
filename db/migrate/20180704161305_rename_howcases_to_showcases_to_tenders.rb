class RenameHowcasesToShowcasesToTenders < ActiveRecord::Migration[5.2]
  def change
    remove_column :tenders, :howcases, :text
    add_column :tenders, :showcases, :text
  end
end
