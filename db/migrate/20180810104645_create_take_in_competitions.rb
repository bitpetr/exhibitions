class CreateTakeInCompetitions < ActiveRecord::Migration[5.2]
  # взятие на участие в тендере
  def change
    create_table :take_in_competitions do |t|
      t.references :tender, foreign_key: true
      t.integer :partner_id

      t.timestamps
    end
    add_index :take_in_competitions, :partner_id
  end
end
