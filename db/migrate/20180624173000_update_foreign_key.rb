class UpdateForeignKey < ActiveRecord::Migration[5.2]
  def change
  remove_foreign_key "profiles", "users"
  # remove_foreign_key "questionnaires", "users"
  remove_foreign_key "tenders", "users"

  add_foreign_key "profiles", "users", on_delete: :cascade
  # add_foreign_key "questionnaires", "users", on_delete: :cascade
  add_foreign_key "tenders", "users", on_delete: :cascade
  end
end
