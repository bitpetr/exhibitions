class AddTakenToTakeInCompetitions < ActiveRecord::Migration[5.2]
  def change
    add_column :take_in_competitions, :taken, :boolean, default: false
  end
end
