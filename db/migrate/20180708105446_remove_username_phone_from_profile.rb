class RemoveUsernamePhoneFromProfile < ActiveRecord::Migration[5.2]
  def change
    remove_column :profiles, :username, :string
    remove_column :profiles, :phone, :string
  end
end
