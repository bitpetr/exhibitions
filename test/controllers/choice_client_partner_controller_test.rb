require 'test_helper'

class ChoiceClientPartnerControllerTest < ActionDispatch::IntegrationTest
  test 'should get choice_page' do
    get choice_client_partner_choice_page_url
    assert_response :success
  end
end
