FactoryBot.define do
  factory :review do
    rating 1
    comment 'MyText'
    tender nil
    user nil
  end
end
