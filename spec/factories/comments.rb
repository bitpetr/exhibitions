FactoryBot.define do
  factory :comment do
    body 'MyText'
    ufile 'MyString'
    user nil
    review nil
  end
end
