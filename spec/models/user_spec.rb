require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of :email }
  it { should validate_presence_of :password }
  it { should validate_confirmation_of :password }
  it { should validate_presence_of :name }
  it { should validate_presence_of :phone }
  it { should validate_presence_of :company }
  it { should validate_presence_of :agreement }
  it { should validate_presence_of :purpose }
  it { is_expected.to_not validate_presence_of :partner }
end
