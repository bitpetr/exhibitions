require 'rails_helper'

RSpec.feature 'Users', type: :feature do
  let(:user) { create(:user) }

  scenario 'Зарегистрированный клиент входит в систему' do
    sign_in(user)

    expect(page).to have_content 'Вход в систему выполнен.'
    expect(current_path).to eq cabinet_path
  end

  scenario 'Не зарегистрированный пользователь пытается войти' do
    visit  new_user_session_path
    fill_in 'E-mail', with: 'aaa@2.a'
    fill_in 'Пароль', with: '111111'
    click_on 'Авторизация'

    expect(page).to have_content 'Неверный E-mail или пароль.'
    expect(current_path).to eq new_user_session_path
  end
end
